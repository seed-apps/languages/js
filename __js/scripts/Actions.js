//========================================================

class Print extends Do {

//========================================================
    //----------------------------------------------------
    onSetup(data){
    //----------------------------------------------------
        super.onSetup(data);
        this.add_variable("text","..");
    }
    //----------------------------------------------------
    onCall(data){
    //----------------------------------------------------
        console.log("<<<<<<<<<<< "+this.path()+"   >>>>>>>>"+this.text);
        this.done();
    }
    //----------------------------------------------------
}
//========================================================

class Call extends Do {

//========================================================
    //----------------------------------------------------
    onSetup(data){
    //----------------------------------------------------
        super.onSetup(data);
        this.add_variable("select","..");
        this.add_variable("event","call");
        this.add_variable("r",false);
    }
    //----------------------------------------------------
    onCall(data){
    //----------------------------------------------------

        var node=this.search_path(this.select);
        node.executeEvent(this.event,this.onCallDone.bind(this));
    }
    //----------------------------------------------------
    onCallDone(obj,event){
    //----------------------------------------------------

        this.done();

    }
    //----------------------------------------------------
}
//========================================================

class Load_Xml extends Do {

//========================================================
    //----------------------------------------------------
    onSetup(data){
    //----------------------------------------------------
        super.onSetup(data);
        this.add_variable("url");
        this.add_variable("node","..");
        this.add_variable("nodename",null);
        this.add_variable("format","obj");
    }
    //----------------------------------------------------
    onCall(){
    //----------------------------------------------------
        this.LoadFile(this.get_url(),this.onFileLoaded,this.format);

    }
    //----------------------------------------------------
    onFileLoaded(data){
    //----------------------------------------------------
        var node=data.response;
        node.attach(this.search_path(this.node));
        if(this.nodename!=null){
            node.name=this.nodename;
        }
        node.setup();
        if ('executeEvent' in node){
            node.executeEvent("setup",this.onSetupDone,true);
        }
        this.done();

    }
    //----------------------------------------------------

    get_url(){

    //----------------------------------------------------
        return this.url;;
        //if(this.url.startsWith('/') || this.url.startsWith('.')){
        //    return this.url;
       // }else{
        //    return this.root().url+"/"+this.url;
        //}
    }
    //----------------------------------------------------

}
//========================================================

