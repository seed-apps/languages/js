
//========================================================

class SelectFirst extends SceneElement {

//========================================================
    //----------------------------------------------------
    onSetup(data){
    //----------------------------------------------------

        super.onSetup(data);
        this.add_variable("attr",null);
        this.add_variable("value",null);
    }
    //----------------------------------------------------
    draw(div,selected){

    //----------------------------------------------------
        var node;
        if(this.attr!=null){
            node=selected.children.by_attr_value(this.attr,this.value).first();
        }else{
            node=selected.first();

        }

        if(node){
            super.draw(div,node);
        }
    }
    //----------------------------------------------------

}
//========================================================
