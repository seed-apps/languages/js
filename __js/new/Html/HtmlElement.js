import {TreeNode} from './../Tree/TreeNode.js';
//======================================================
export class HtmlElement extends TreeNode {
//======================================================

    //---------------------------------------------------
    make() {
    //---------------------------------------------------
        console.log("make",this);
        this.nodes=this.onGetNode();

        if(this.data.class){
           this.nodes.addClass(this.data.class);
        }

        if(this.data.id){
           this.nodes.attr("id",this.data.id);
        }

        if(this.data.style){
            this.nodes.attr("style",this.data.style);
        }

        if(this.data.text){
            this.nodes.text(this.data.text);
        }

        if(this.data.onclick){
            this.nodes.attr("onclick",this.data.onclick);
        }

       if(this.parent != null){
            this.nodes.appendTo(this.parent.nodes);
        }

        var lst=this.by_class(TreeNode);
        for (const child in lst) {

            if(lst[child].make){
                var elt=lst[child].make();
                if(elt){
                    elt.appendTo(this.nodes);
                }
            }
        } 
        return this.nodes;
     }
    //---------------------------------------------------
    onGetNode() {
    //---------------------------------------------------
        if(this.data.select){
            return $(this.data.select);
        }else if(this.data.tag){
            return $("<"+this.data.tag+">");
            //return node;
        }else{
            return $("<div>");

        }
     }
    //---------------------------------------------------
}
//======================================================
