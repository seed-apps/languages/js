import {HtmlElement} from './HtmlElement.js';
  import {TreeNode} from './../Tree/TreeNode.js';

//======================================================
export class SelectOption extends TreeNode {
//======================================================

}
//======================================================
export class Select extends HtmlElement {
//======================================================

    //---------------------------------------------------
    onGetNode() {
    //---------------------------------------------------

        var node=$("<div>")
                    .addClass("form-floating");

        var input=$("<select>").attr("name",this.data.name)
                    .attr("id","input_"+this.id)
                    .attr("placeholder",this.data.type)
                    .addClass("form-select")
                    .appendTo(node);

        $("<option selected>")
                    .appendTo(input);

        var lst=this.by_class(SelectOption);
        for (const child in lst) {
            $("<option>").attr("value",lst[child].data.value)
                        .text(lst[child].data.label)
                        .appendTo(input);
        } 


        $("<label>").text(this.data.label)
                    .attr("for","input_"+this.id)
                    .appendTo(node);

        return node;
      }

    //---------------------------------------------------
    get_value() {
    //---------------------------------------------------

            return this.nodes.find("input").val();
      }
    //---------------------------------------------------
}
//======================================================
export class Input extends HtmlElement {
//======================================================


    //---------------------------------------------------
    onGetNode() {
    //---------------------------------------------------
        var node=$("<div>")
                    .addClass("form-floating");

        var input=$("<input>").attr("name",this.data.name)
                    .attr("id","input_"+this.id)
                    .attr("type",this.data.type)
                    .attr("placeholder",this.data.type)
                    .addClass("form-control")
                    .appendTo(node);

        if(this.data.default){
           input.attr("value",this.data.default);
        }

        $("<label>").text(this.data.label)
                    .attr("for","input_"+this.id)
                    .appendTo(node);

        return node;
      }

    //---------------------------------------------------
    get_value() {
    //---------------------------------------------------

            return this.nodes.find("input").val();
      }
    //---------------------------------------------------
}
//======================================================
export class HiddenInput extends Input {
//======================================================

    //---------------------------------------------------
    onGetNode() {
    //---------------------------------------------------

        var input=$("<input>")
                    .attr("name",this.data.name)
                    .attr("type","hidden")
                    .attr("value",this.data.value);

        return input;
      }

    //---------------------------------------------------
    get_value() {
    //---------------------------------------------------

            return this.nodes.find("input").val();
      }
    //---------------------------------------------------
}
//======================================================
export class Button extends HtmlElement {
//======================================================


    //---------------------------------------------------
    onGetNode() {
    //---------------------------------------------------
            var node =$("<button>");
            node.attr("type",this.data.type)
                        .text(this.data.text);

            //node.text(this.data.text).click(this.onClick.bind(this));
            if(this.data.onclick){
                node.attr("onclick",this.data.onclick+";return false;");
            }

            return node;
      }
    //---------------------------------------------------
    onClick() {
    //---------------------------------------------------
        console.log("click");
        var data=this.parent.serializeArray();
        console.log(this.data.name,data);
    }
    //---------------------------------------------------
}
//======================================================
export class Form extends HtmlElement {
//======================================================

    //---------------------------------------------------
    onGetNode() {
    //---------------------------------------------------
        var node=$("<form>")
                    .addClass("MyForms")
                    .attr("id",this.data.name);


        if(this.data.method){
            node.attr("action",this.data.action);
            node.attr("method",this.data.method);

        }else{
            node.attr("onsubmit",";return false;");
        }

        $("<h5>").text(this.data.title).appendTo(node);
        $("<p>").text(this.data.description).appendTo(node);
        return node;
     }

    //---------------------------------------------------
    serializeArray() {
    //---------------------------------------------------
        var result={};
        var lst=this.by_class(Input);
        for (const child in lst) {
            result[lst[child].data.name]=lst[child].get_value();
        } 
        return result;
    }
    //---------------------------------------------------
}
//======================================================

