  import {Object3D} from './Object3D.js';
  import {Polar} from './Helpers.js';
  import * as THREE from '/seed-env/__static/threejs/three.module.js';
import { OrbitControls } from '/seed-env/__static/threejs/OrbitControls.js';
//======================================================
export class OrbitCamera extends Object3D {
//======================================================
//https://github.com/mrdoob/three.js/blob/dev/examples/misc_controls_orbit.html#L66

    //---------------------------------------------------
    onBuild3d() {
    //---------------------------------------------------
        var camera = new THREE.PerspectiveCamera( 50, this.parent.width /  this.parent.height, 0.01,parseInt(this.data.r) );
        this.controls = new OrbitControls( camera, this.parent.renderer.domElement );
        this.controls.listenToKeyEvents( window ); // optional

        //controls.addEventListener( 'change', render ); // call this only in static scenes (i.e., if there is no animation loop)

        this.controls.enableDamping = true; // an animation loop is required when either damping or auto-rotation are enabled
        this.controls.dampingFactor = 0.05;

        this.controls.screenSpacePanning = false;

        this.controls.minDistance = 10;
        this.controls.maxDistance = 1000;

        //this.controls.maxPolarAngle = Math.PI / 2;
        //camera.rotateX(Math.Pi/2);
        camera.position.z = 1;
        return camera;
  }
    //---------------------------------------------------
    onDebugEnter() {
    //---------------------------------------------------
        console.log("test OrbitCamera");
        this.helper=new Polar(this,{r:0.95*this.data.r});
        this.helper.make();
    }
    //---------------------------------------------------
    onDebugExit() {
    //---------------------------------------------------

        this.helper.destroy();
    }
    //---------------------------------------------------
    onRun(time) {
    //---------------------------------------------------
        //console.log("test");
        this.parent.camera=this.object3d;
        //this.parent.renderer.render( this.parent.scene, this.object3d );
        this.controls.update();
    }
    //---------------------------------------------------
}
//======================================================
