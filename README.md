## webpack

https://webpack.js.org/guides/getting-started/
https://www.alsacreations.com/tuto/lire/1754-debuter-avec-webpack.html

## three.js et webpack

https://dev.to/dharmi/creating-the-webpack-required-for-threejs-42fi
https://github.com/aakatev/three-js-webpack/blob/master/webpack.config.js
https://abi-web-app-documentation.readthedocs.io/en/latest/three/tutorial_2/threejs_with_webpack.html

## loadash

https://lodash.com/

Lodash makes JavaScript easier by taking the hassle out of working with arrays, numbers, objects, strings, etc. Lodash’s modular methods are great for:

    Iterating arrays, objects, & strings
    Manipulating & testing values
    Creating composite functions
