<?php
include($_SERVER["DOCUMENT_ROOT"]."/seed-env/plugins/php/settings.php");

$application=new HttpMain(null,["path"=>$PROGRAM_PATH]);
new Import($application,["url"=>"/settings.xml"]);
new InitXmlFiles($application,["event"=>"setup","filter"=>"/*.xml"]);

$application->setup();




if($_SERVER['REQUEST_METHOD'] == 'POST') {
        $request=$_POST;
}else{
        $request=$_GET;
}


if( array_key_exists("theme",$request) ){
    $theme=$request["theme"];
}else{
    $theme="topmenu";
}


if( array_key_exists("data",$request) ){
    $data="./data.php?path=".$request["dir"];

}else if( array_key_exists("select",$request) ){
    $data_path=$application->find($request["select"])->get("path");
    $data="./data.php?path=".$data_path;

}else{
$data="./data.php";

}

$node=$application;
    $application->header($node,$node);
?>


<div id="main" ></div>
<script src="/seed-env/__static/blackboard/blackboard-min.js"></script>
<script>
DEBUG_LEVEL=5;

    var root="/";
    var path="/";

    var main=new Bd_Main("body","/seed-env/__gui/<?=$theme?>/index.xml","<?=$data?>");


function script(){
    console.log("ok");
    $("#main").empty();
    main.run();

};

//setTimeout(script, 200);
</script>
<?php
    $application->footer($node);
?>
